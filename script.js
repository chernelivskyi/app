///exercise №1///

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getQuarterHour() {
    let minutes = getRandomInt(0, 59);

    if (minutes > 0 && minutes <= 15) {
        console.log(`${minutes} its first `)
    } else if (minutes > 15 && minutes <= 30) {
        console.log(`${minutes} its second `)
    } else if (minutes > 30 && minutes <= 45) {
        console.log(`${minutes} its third `)

    } else {
        console.log(`${minutes} its fourth`)
    }


}

getQuarterHour()

///exercise №2///

let lang = 'ru';
if (lang === 'en') {
    console.log(['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn'])
} else if (lang === 'ru') {
    console.log(['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд'])
}


switch (lang) {
    case "ru":
        console.log('пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд');
        break;
    case "en":
        console.log('mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn');
        break;
}

let languages = {
    en: ['mn', 'ts', 'wd', 'th', 'fr', 'st', 'sn'],
    ru: ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'нд']
}
console.log(languages[lang])

///exercise №3///

let a = []
let d = 1
for (let i = 0; i < 10; i++) {
    a.push(Math.floor(Math.random() * 11) + 1)
    d *= a [i]
}
console.log(d)
console.log(a);

